import requests, os
from bs4 import BeautifulSoup as bs

proxyn=1
with open("workingproxy.txt", "r") as file:
	for ip in file:
		print("trying proxy {}...".format(proxyn))
		try:ip=ip.split('\n')[0]
		except:pass
		if not os.path.isdir("./scenes"):os.mkdir("./scenes")
		firstpage='http://up.illusion.jp/phome_upload/scene/index.php?cPath=26'
		try:
			r=requests.get(firstpage, proxies={'http':ip})
			soup=bs(r.text, 'html.parser')
			pages=[ p["href"] for p in soup.find(attrs={"class":"pagepn"}).findAll(attrs={"class":"pageResults"}) ]
			pages.insert(0, firstpage)
			n=1
			scenen=1
			for url in pages:
				print("loading page {}...".format(n))
				page=requests.get(url, proxies={'http':ip})
				soup=bs(page.text, 'html.parser')
				uc_img=soup.findAll(attrs={'class':'uc_img'})
				for x in uc_img:
					filename=x.find('img')['src'].split("/")[-1]
					url=x.find('a')['href']
					if os.path.isfile("./scenes/"+filename):continue
					print("downloading scene {}".format(scenen))
					scene=requests.get(url, proxies={'http':ip})
					with open("./scenes/"+filename, 'wb') as f:
						f.write(scene.content)
					scenen+=1
				n+=1
		except:
			proxyn+=1
			continue
		else:break